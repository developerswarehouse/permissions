<?php

namespace DevelopersWarehouse\Permissions;

/**
 * This Trait sets up a one to many relationship.
 */
trait ManyPermissions
{

    protected $permissionsPivot = "pivot_permission";

    protected $permissionsPivotLocal = "object_id";

    /**
     * Define the permissions relationship.
     * @return [type] [description]
     */
    public function permissions()
    {
        return $this->belongsToMany(\DevelopersWarehouse\Permissions\Models\Permission::class, $this->permissionsPivot, $this->permissionsPivotLocal);
    }
}
