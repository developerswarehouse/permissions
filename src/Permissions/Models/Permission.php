<?php

namespace DevelopersWarehouse\Permissions\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config("permissions.database.table");
    }
}
