<?php

namespace DevelopersWarehouse\Permissions;

/**
 * This Trait allows you to setup a simple permissions structure. Ideal for a small user base.
 * Each user has their own collection of permissions; the structure to handle this will need creating within you own application.
 */
trait HasPermission
{

    /**
     * Check the relationship for a permissions key (string).
     * @param  [type]  $key [description]
     * @return boolean      [description]
     */
    public function hasPermission($key)
    {
        return $this->permissions->where('key',$key)->count();
    }

    /**
     * Check for multiple permissions at once.
     * @param  [type]  $keys [description]
     * @return boolean       [description]
     */
    public function hasPermissions($keys)
    {
        foreach((array)$keys as $permission_key) {
            if (!$this->hasPermission($permission_key)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check multiple permission keys, return true is ONE is found.
     * @param  [type]  $keys [description]
     * @return boolean       [description]
     */
    public function hasPermissionIn($keys)
    {
        foreach((array)$keys as $permission_key) {
            if ($this->hasPermission($permission_key)) {
                return true;
            }
        }
        return false;
    }
}
