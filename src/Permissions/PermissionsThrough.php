<?php

namespace DevelopersWarehouse\Permissions;

/**
 * This Trait allows you to setup a simple permissions structure. Ideal for a small user base.
 * Each user has their own collection of permissions; the structure to handle this will need creating within you own application.
 */
trait PermissionsThrough
{
    /**
     * Define the permissions relationship.
     * @return [type] [description]
     */
    public function getPermissionsAttribute()
    {
        $throughRelationship = $this->permissionsThrough;

        $permissions = collect();
        $through = $this->{$throughRelationship};

        foreach ($through as $relation) {
            $permissions = $permissions->merge($relation->permissions);
        }

        return $permissions;
    }
}
