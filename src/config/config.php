<?php

return [

    /**
     * Where are the permissions stored in the database.  The relationship will
     * need to be kept to the database, but if you wish you can load the
     * permissions from:
     *
     * database, or array
     */
    "storage" => "database",

    /**
     * If Database storage.
     * Where the permissions are stored in the database.
     */
    "database" => [
        "table" => "permissions",
    ],

     /**
      * Hardcode the permissison values to an array.
      * @todo The config is in development.
      */
    // "array" => [
    //     "user_can_do_something" => "Ability to do something",
    // ],

];
